//
//  RegisterViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import Pastel
import TransitionButton

class RegisterViewController: UIViewController {

    @IBOutlet var textfields: [UITextField]!
    
    @IBOutlet weak var pastelView: PastelView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var signUpButton: TransitionButton!
    
    let picker = UIPickerView()
    lazy var areaArray = [Area]()
    var selectedArea = Area()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAnimation()
        setTextFields()
        getAreas()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false

    }
    
    func setTextFields() {
        for textfield in textfields {
            textfield.layer.cornerRadius = 3
            textfield.setLeftPaddingPoints(10)
            textfield.backgroundColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4)
        }
        nameTextField.attributedPlaceholder = NSAttributedString(string: "Full Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        mobileTextField.attributedPlaceholder = NSAttributedString(string: "Mobile", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        areaTextField.attributedPlaceholder = NSAttributedString(string: "Area", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
    }
    
    func setAnimation() {
        // Custom Direction
        pastelView.startPastelPoint = .bottomRight
        pastelView.endPastelPoint = .topLeft
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([UIColor(red:0.00, green:0.39, blue:0.00, alpha:1.0),
                              UIColor(red:0.49, green:0.99, blue:0.00, alpha:1.0),
                              UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0),
                              UIColor(red:0.20, green:0.80, blue:0.20, alpha:1.0)])
        //pastelView.setColors([UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
//                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
//                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
//                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0),
//                              UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
//                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
//                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0)])
        
        pastelView.startAnimation()
    }
    
    @IBAction func signUpButtonTapped(_ sender: TransitionButton) {
        if Common.sharedInst.haveInternet() {
            if checkData() {
                registerUser()
            } else {
                self.showAlert(message: no_valid)
            }
        } else {
            self.showAlert(message: no_internet)
        }
    }
    
    func checkData() -> Bool {
        guard let name = nameTextField.text, let email = emailTextField.text, let mobile = mobileTextField.text, let password = passwordTextField.text else { return false }
        
        if name.isEmpty || email.isEmpty || mobile.isEmpty || password.isEmpty {
            //show error
            return false
        } else {
            return Common.sharedInst.validateEmailString(email: email)
        }
    }
    func registerUser() {
        guard let name = nameTextField.text, let email = emailTextField.text, let mobile = mobileTextField.text, let password = passwordTextField.text else { return }
        signUpButton.startAnimation()
        
        var details = [String: Any]()
        details["email"] = email
        details["password"] = password
        details["mobile"] = mobile
        details["name"] = name
        details["area"] = selectedArea.id
        
        Service.shared.request(webName: "register", whoswebcall: false, APIType: .post, params: details, completionBlock: { (response) in
            print(response)
            do {
                let userData = try jsonDecoder.decode(User.self, from: response as! Data)
              
                Common.sharedInst.saveUserData(data: userData)
                
                self.signUpButton.stopAnimation(animationStyle: .expand, revertAfterDelay: 1.0, completion: {
                    self.performSegue(withIdentifier: "takeUserData", sender: self)
                })
                

            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            
        }) { (error) in
            let errorData = error as! [String: [String]]
            if let errors = errorData["errors"] {
                self.showAlert(message: errors.compactMap{ $0 }.joined(separator: "\n"))
            }
            self.signUpButton.stopAnimation()
        }
    }
    
    func getAreas() {
        Loader.show()
        let webName = "areas/all"
        
        Service.shared.request(webName: webName, whoswebcall: false, APIType: .get, params: [:], completionBlock: { (response) in
            do {
                self.areaArray = try jsonDecoder.decode([Area].self, from: response as! Data)
            } catch {
                print(error)
            }
            Loader.dismiss()
        }) { (error) in
            print(error)
            Loader.dismiss()

        }
    }
}



extension RegisterViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setupPickerView(in: textField)
    }
    
    @objc func doneClick() {
        areaTextField.resignFirstResponder()
    }
    
}

extension RegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return areaArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return areaArray[row].area
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if areaArray.count > 0 {
            areaTextField.text = areaArray[row].area
            selectedArea = areaArray[row]
        }
    }
    
    func setupPickerView(in textField: UITextField) {
        
        picker.delegate = self
        picker.dataSource = self
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        textField.inputView = picker
        
    }
}
