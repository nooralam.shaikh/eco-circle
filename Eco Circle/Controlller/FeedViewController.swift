//
//  FirstViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import Floaty
import AVKit
import Kingfisher

enum Post: String {
    case image = "image"
    case video = "video"
}

class FeedViewController: BaseUIViewController {

    @IBOutlet weak var feedTableView: UITableView!
    
    lazy var postViewController: PostViewController = storyboard?.instantiateViewController(withIdentifier: "PostViewController") as! PostViewController
    lazy var createImagePostViewController: CreateImagePostViewController = storyboard?.instantiateViewController(withIdentifier: "CreateImagePostViewController") as! CreateImagePostViewController
    lazy var createVideoPostViewController: CreateVideoPostViewController = storyboard?.instantiateViewController(withIdentifier: "CreateVideoPostViewController") as! CreateVideoPostViewController
    lazy var profileViewCotroller = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
    
    let postArray: [Post] = [.image, .video]
    var feedArray = [Posts]()
    var isFromLike = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.setNavigationHead(heading: "Eco Circle")
        setBackButton()
        setFloatingMenu()
        if Common.sharedInst.haveInternet() {
            getFeed()
        } else {
            self.showAlert(message: no_internet)
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Common.sharedInst.haveInternet() {
            getFeed()
        } else {
            self.showAlert(message: no_internet)
        }

    }
    
    override func setBackButton() {
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")

        if isLoggedIn {
            self.navigationController?.navigationItem.leftBarButtonItem = backButton
        } else {
            self.navigationController?.navigationItem.leftBarButtonItem = nil
        }
    }
    
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setFloatingMenu() {
        let floaty = Floaty()
        floaty.buttonColor = COLOR_FOREST_GREEN
        floaty.frame.size.height = 25
        floaty.frame.size.width = 25
//        floaty.itemTitleColor = BLUE_COLOR
        floaty.plusColor = UIColor.white
        floaty.openAnimationType = .pop
        floaty.overlayColor = UIColor.black.withAlphaComponent(0.6)
        
        floaty.addItem("Record Video", icon: UIImage()) { (_) in
            print("Record Video Tapped")
            self.navigationController?.pushViewController(self.createVideoPostViewController, animated: true)
        }
        floaty.addItem("Capture Image", icon: UIImage()) { (_) in
            self.navigationController?.pushViewController(self.createImagePostViewController, animated: true)
        }
        
        floaty.items.forEach {
            $0.titleLabel.textColor = COLOR_FOREST_GREEN
            $0.titleLabel.font = Common.sharedInst.getFont(type: .medium, size: 14)
            $0.titleLabel.textAlignment = .center
            $0.titleLabel.backgroundColor = UIColor.white
            $0.titleLabel.layer.cornerRadius = 5
        }
        self.view.addSubview(floaty)
    }

}

extension FeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feedCell", for: indexPath) as! FeedTableViewCell
        
        cell.selectionStyle = .none
        cell.commentButton.addTarget(self, action: #selector(reportTapped(_:)), for: .touchUpInside)
        cell.commentButton.tag = indexPath.row
        cell.captionLabel.text = feedArray[indexPath.row].title ?? ""
        cell.likeButton.addTarget(self, action: #selector(likeButtonAction(_:)), for: .touchUpInside)
        cell.likeButton.tag = indexPath.row
        cell.playButton.tag = indexPath.row
        guard let postType = Post(rawValue: feedArray[indexPath.row].type ?? ""), let url = URL(string: feedArray[indexPath.row].url ?? ""), let liked = feedArray[indexPath.row].liked, let likes = feedArray[indexPath.row].likes, let name = feedArray[indexPath.row].name else { return UITableViewCell() }
        
        cell.userImageView.isUserInteractionEnabled = true
        cell.userImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showProfile)))
        cell.userImageView.tag = indexPath.row
        
        if let createdUserProfile = feedArray[indexPath.row].picture {
            let createdUserProfileURL = URL(string: createdUserProfile)
            cell.userImageView.kf.setImage(with: createdUserProfileURL)
        } else {
            cell.userImageView.image = UIImage(named: "man")
        }
        if let createdAt = feedArray[indexPath.row].created_at {
            cell.postCreateLabel.text = createdAt
            cell.postCreateLabel.isHidden = false
        } else {
            cell.postCreateLabel.isHidden = true
        }
        cell.userNameLabel.text = name
        cell.cellInfoLabel.text = "\(likes) Smiles"
        cell.likeButton.isSelected = liked == 0 ? false : true

        switch postType {
//        case .text:
//            cell.previewHeight.constant = 0
//            cell.playImageView.isHidden = true
        case .image:
            cell.postThumbnailImageView.kf.setImage(with: url)
            cell.playImageView.isHidden = true
            cell.overlayView.isHidden = true
            cell.playButton.isUserInteractionEnabled = false
            
        case .video:
            let stringUrl = URL(string: url.absoluteString.replacingOccurrences(of: ".mov", with: ".png") )
            cell.postThumbnailImageView.kf.setImage(with: stringUrl)
            cell.playButton.isUserInteractionEnabled = true
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action: #selector(playVideo(_:)), for: .touchUpInside)
            print("in case")
            cell.playImageView.isHidden = false
            cell.overlayView.isHidden = false
        }
        
        return cell
    }
    
    
}

extension FeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension FeedViewController {
    @objc func playVideo(_ sender: UIButton) {
        print(sender.tag)
        if let videoString = feedArray[sender.tag].url {
            let videoURL = URL(string: videoString)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        
    }
    
    @objc func reportTapped(_ sender: UIButton) {
        // Create the alert controller
        let alertController = UIAlertController(title: "Report", message: "Message", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.doReport(at: sender.tag)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func likeButtonAction(_ sender: UIButton) {
        sender.isSelected = true
        doLike(at: feedArray[sender.tag].id ?? 0)
    }
    
    @objc func showProfile(at sender: UITapGestureRecognizer) {
        let touch = sender.location(in: feedTableView)
        if let indexPath = feedTableView.indexPathForRow(at: touch) {
            profileViewCotroller.isFromFeed = true
            profileViewCotroller.userId = feedArray[indexPath.row].user_id ?? 0
            self.navigationController?.pushViewController(profileViewCotroller, animated: true)
        }
        
    }
}

extension FeedViewController {
    func getFeed() {
        if !isFromLike { Loader.show() }
        Service.shared.request(webName: "posts/", whoswebcall: false, APIType: .get, params: [:], completionBlock: { (response) in
            print(response)
            do {
                let feedData = try jsonDecoder.decode(Feed.self, from: response as! Data)
                self.feedArray = feedData.posts ?? [Posts]()
                self.feedTableView.reloadData()
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            Loader.dismiss()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
            Loader.dismiss()
        }
    }
    
    func doLike(at id: Int) {
        Service.shared.request(webName: "posts/like/\(id)", whoswebcall: false, APIType: .post, params: [:], completionBlock: { (response) in
            print(response)
            self.isFromLike = true
            self.getFeed()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
        }
    }
    
    func doReport(at id: Int) {
        Service.shared.request(webName: "posts/report/\(id)", whoswebcall: false, APIType: .post, params: [:], completionBlock: { (response) in
            print(response)
            self.showAlert(message: "Post Reported")
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
        }
    }
}
