//
//  MyTasksViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 21/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

class MyTasksViewController: UIViewController {

    @IBOutlet weak var taskTableView: UITableView!
    
    lazy var taskDetailViewController = storyboard?.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
    var taskArray = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        getTasks()
    }
    
    
    func getTasks() {
        Loader.show()
        Service.shared.request(webName: "tasks/my-tasks", whoswebcall: false, APIType: .get, params: [:], completionBlock: { (response) in
            print(response)
            do {
                self.taskArray = try jsonDecoder.decode([Task].self, from: response as! Data)
                self.taskTableView.reloadData()
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            Loader.dismiss()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
            Loader.dismiss()
        }
    }
}



extension MyTasksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskTableViewCell
        
        cell.ngoNameLabel.text = taskArray[indexPath.row].ngo_name
        cell.taskNameLabel.text = taskArray[indexPath.row].title
        cell.descriptionLabel.text = "Description \n \(taskArray[indexPath.row].description ?? "")"
        cell.participantLabel.text = "Total Participation: \(taskArray[indexPath.row].participated ?? 0)"
        
        cell.selectionStyle = .none
        return cell
    }
    
    
}

extension MyTasksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        taskDetailViewController.task = taskArray[indexPath.row]
        taskDetailViewController.isUserTask = true
        self.navigationController?.pushViewController(taskDetailViewController, animated: true)
    }
    
}

