//
//  TransactionProgressViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 21/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import UICircularProgressRing

class TransactionProgressViewController: UIViewController {

    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var usedAtLabel: UILabel!
    @IBOutlet weak var redeemLabek: UILabel!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var progressView: UICircularProgressRing!
    
    var transaction: Transaction!
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Transaction Detail"
        return title
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView.maxValue = 60
        progressView.innerRingColor = COLOR_FOREST_GREEN
        self.navigationItem.titleView = titleLabel
        // Do any additional setup after loading the view.
    }
    
    func setTransactionData() {
        amountLabel.text = "₹ \(transaction.amount ?? 0)"
        usedAtLabel.text = "Used At \(transaction.used_at ?? "")"
        transactionLabel.text = "Transaction ID: \(transaction.id ?? 0)"
        redeemLabek.text = "Redeem Time \(transaction.created_at ?? "")"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setTransactionData()
        startProgressView()
    }
    
    func startProgressView() {
        
        progressView.startProgress(to: 60, duration: 60.0) {
            self.progressView.resetProgress()
            self.backTwo()
//            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func backTwo() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
