//
//  TaskDetailViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 21/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import Floaty

class TaskDetailViewController: UIViewController {

    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var ngoNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var totalParticipantsLabel: UILabel!
    @IBOutlet weak var taskButton: NSButton!
    
    lazy var createVideoPostViewController = storyboard?.instantiateViewController(withIdentifier: "CreateVideoPostViewController") as! CreateVideoPostViewController
    lazy var createImagePostViewController = storyboard?.instantiateViewController(withIdentifier: "CreateImagePostViewController") as! CreateImagePostViewController
    
    var task: Task!
    var isUserTask = false
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Task Detail"
        return title
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isUserTask {
            taskButton.isHidden = true
            setFloatingMenu()
        }
        self.navigationItem.titleView = titleLabel
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        ngoNameLabel.text = task.ngo_name
        taskNameLabel.text = task.title
        descriptionLabel.text = "Description \n \(task.description ?? "")"
        instructionLabel.text = "Instructions \n \(task.instructions ?? "")"
        totalParticipantsLabel.text = "Total Participation: \(task.participated ?? 0)"
    }
    
    func setFloatingMenu() {
        let floaty = Floaty()
        floaty.buttonColor = COLOR_FOREST_GREEN
        floaty.frame.size.height = 25
        floaty.frame.size.width = 25
        //        floaty.itemTitleColor = BLUE_COLOR
        floaty.plusColor = UIColor.white
        floaty.openAnimationType = .pop
        floaty.overlayColor = UIColor.black.withAlphaComponent(0.6)
        
        floaty.addItem("Record Video", icon: UIImage()) { (_) in
            print("Record Video Tapped")
            self.createVideoPostViewController.isFromTask = true
            self.createVideoPostViewController.taskId = self.task.id ?? 0
            self.navigationController?.pushViewController(self.createVideoPostViewController, animated: true)
        }
        
        floaty.addItem("Capture Image", icon: UIImage()) { (_) in
            self.createImagePostViewController.isFromTask = true
            self.createImagePostViewController.taskId = self.task.id ?? 0
            self.navigationController?.pushViewController(self.createImagePostViewController, animated: true)
        }
        
        floaty.items.forEach {
            $0.titleLabel.textColor = COLOR_FOREST_GREEN
            $0.titleLabel.font = Common.sharedInst.getFont(type: .medium, size: 14)
            $0.titleLabel.textAlignment = .center
            $0.titleLabel.backgroundColor = UIColor.white
            $0.titleLabel.layer.cornerRadius = 5
        }
        self.view.addSubview(floaty)
    }
    
    @IBAction func participateButtonTapped(_ sender: UIButton) {
        if isUserTask {
            
        } else {
            let id = task.id
            Loader.show()
            Service.shared.request(webName: "tasks/participate/\(id ?? 0)", whoswebcall: false, APIType: .post, params: [:], completionBlock: { (response) in
                Loader.dismiss()
                self.showAlert(message: "You Have Been Participated in \(self.task.title ?? "") Successfully.")
            }) { (error) in
                let errorData = error as! [String: String]
                if let errorString = errorData["error"] {
                    self.showAlert(message: errorString)
                }
                Loader.dismiss()
            }
        }
        
    }
    
}
