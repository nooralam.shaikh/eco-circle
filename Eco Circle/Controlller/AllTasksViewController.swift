//
//  AllTasksViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 21/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

class AllTasksViewController: UIViewController {

    @IBOutlet weak var taskTableView: UITableView!
    var taskArray = [Task]()
    lazy var taskDetailViewController = storyboard?.instantiateViewController(withIdentifier: "TaskDetailViewController") as! TaskDetailViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTasks()
        // Do any additional setup after loading the view.
    }
    func getTasks() {
        Loader.show()
        Service.shared.request(webName: "tasks/", whoswebcall: false, APIType: .get, params: [:], completionBlock: { (response) in
            print(response)
            do {
                self.taskArray = try jsonDecoder.decode([Task].self, from: response as! Data)
                self.taskTableView.reloadData()
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            Loader.dismiss()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
            Loader.dismiss()
        }
    }
}



extension AllTasksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskTableViewCell
        
        cell.ngoNameLabel.text = taskArray[indexPath.row].ngo_name
        cell.taskNameLabel.text = taskArray[indexPath.row].title
        cell.descriptionLabel.text = "Description \n \(taskArray[indexPath.row].description ?? "")"
        cell.participantLabel.text = "Total Participation: \(taskArray[indexPath.row].participated ?? 0)"
        
        cell.selectionStyle = .none
        return cell
    }
    
    
}

extension AllTasksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        taskDetailViewController.task = taskArray[indexPath.row]
        self.navigationController?.pushViewController(taskDetailViewController, animated: true)
    }
    
}
