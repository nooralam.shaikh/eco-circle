//
//  RedeemViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 19/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

class RedeemViewController: UIViewController {

    @IBOutlet weak var amountTextfield: UITextField!
    @IBOutlet weak var pointLabel: UILabel!
    var amountInt = Int()
    var userProfile: Profile!
    
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Redeem"
        return title
    }()
    
    lazy var scanQRViewController = storyboard?.instantiateViewController(withIdentifier: "ScanQRViewController") as! ScanQRViewController
    lazy var transactionProgressViewController = storyboard?.instantiateViewController(withIdentifier: "TransactionProgressViewController") as! TransactionProgressViewController
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = titleLabel
        amountTextfield.layer.cornerRadius = 3
        amountTextfield.setLeftPaddingPoints(10)
        amountTextfield.backgroundColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserData()
    }
    
    @IBAction func scanButtonTapped(_ sender: UIButton) {
        guard let amount = amountTextfield.text, let reward = userProfile.rewards else { return }
        if amount.isEmpty {
            self.showAlert(message: "Please Enter Amount")
        } else {
            if let intAmount = Int(amount) {
                amountInt = intAmount
                if intAmount > reward {
                    self.showAlert(message: "Please Enter Amount less than Your Rewards")
                } else {
                    scanQRViewController.delegate = self
                    self.navigationController?.pushViewController(scanQRViewController, animated: true)
                }
            }
        }
        
    }
    
    func refreshData() {
        if let reward = userProfile.rewards {
            pointLabel.numberOfLines = 0
            pointLabel.text = "💰 You have \(reward) reward points \n 1 Reward Point = 1 ₹"

        } else {
            pointLabel.text = "You have " + "0"

        }
    }
}

extension RedeemViewController {
    func getUserData(){
        Loader.show()
        Service.shared.request(webName: "profile", whoswebcall: false, APIType: .get, params: [:], completionBlock: { (response) in
            print(response)
            do {
                self.userProfile = try jsonDecoder.decode(Profile.self, from: response as! Data)
                self.refreshData()
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            Loader.dismiss()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
            Loader.dismiss()
        }
    }
    
}

extension RedeemViewController: RedeemPointDelegate {
    func redeemPoints(with code: String) {
        Loader.show()
        let details = ["amount":amountInt, "area": code] as [String : Any]
        
        Service.shared.request(webName: "wallet/redeem", whoswebcall: false, APIType: .post, params: details, completionBlock: { (response) in
            print(response)
            do {
                let transaction = try jsonDecoder.decode(Transaction.self, from: response as! Data)
                self.transactionProgressViewController.transaction = transaction
                self.navigationController?.pushViewController(self.transactionProgressViewController, animated: true)
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            Loader.dismiss()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
            Loader.dismiss()
        }
    }
    
    
}
