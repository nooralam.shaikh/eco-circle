//
//  CreateVidePostViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 20/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

import UIKit
import AVFoundation



class CreateVideoPostViewController: UIViewController, AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        recordedVideoURL = outputFileURL
    }
    
    var recordedVideoURL: URL!
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Record Video"
        return title
    }()
    
    @IBOutlet weak var captionTextField: UITextField!
    @IBOutlet weak var recordButton: UIButton!
    
    @IBOutlet weak var camPreview: UIView!
    
    
    var isFromTask = false
    var taskId = Int()
    
    let cameraButton = UIView()
    
    let captureSession = AVCaptureSession()
    
    let movieOutput = AVCaptureMovieFileOutput()
    
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var activeInput: AVCaptureDeviceInput!
    
    var outputURL: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        if setupSession() {
            setupPreview()
            startSession()
        }
        
//        cameraButton.isUserInteractionEnabled = true
//
//        let cameraButtonRecognizer = UITapGestureRecognizer(target: self, action: #selector(startCapture))
//
//        cameraButton.addGestureRecognizer(cameraButtonRecognizer)
//
//        cameraButton.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
//
//        cameraButton.backgroundColor = UIColor.red
//
//        camPreview.addSubview(cameraButton)
        recordButton.addTarget(self, action: #selector(startCapture), for: .touchUpInside)
        
    }
    func setNavigation() {
        let saveButton = UIBarButtonItem(image: UIImage(named: "checked"), style: .plain, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem  = saveButton
        
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backButtonTapped))
        self.navigationItem.leftBarButtonItem = backButton
        
        
        
        self.navigationItem.titleView = titleLabel
    }
    
    @objc func saveButtonTapped() {
        if Common.sharedInst.haveInternet() {
            if captionTextField.text != "" {
                uploadVideo()
            } else {
                self.showAlert(message: "Enter Caption")
            }
        } else {
            self.showAlert(message: no_internet)
        }
        
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupPreview() {
        // Configure previewLayer
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = camPreview.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        camPreview.layer.addSublayer(previewLayer)
    }
    
    //MARK:- Setup Camera
    
    func setupSession() -> Bool {
        
        captureSession.sessionPreset = AVCaptureSession.Preset.high
        
        // Setup Camera
        let camera = AVCaptureDevice.default(for: .video)
        
        do {
            let input = try AVCaptureDeviceInput(device: camera!)
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                activeInput = input
            }
        } catch {
            print("Error setting device video input: \(error)")
            return false
        }
        
        // Setup Microphone
        let microphone = AVCaptureDevice.default(for: .audio)
        
        do {
            let micInput = try AVCaptureDeviceInput(device: microphone!)
            if captureSession.canAddInput(micInput) {
                captureSession.addInput(micInput)
            }
        } catch {
            print("Error setting device audio input: \(error)")
            return false
        }
        
        
        // Movie output
        if captureSession.canAddOutput(movieOutput) {
            captureSession.addOutput(movieOutput)
        }
        
        return true
    }
    
    func setupCaptureMode(_ mode: Int) {
        // Video Mode
        
    }
    
    //MARK:- Camera Session
    func startSession() {
        
        
        if !captureSession.isRunning {
            videoQueue().async {
                self.captureSession.startRunning()
            }
        }
    }
    
    func stopSession() {
        if captureSession.isRunning {
            videoQueue().async {
                self.captureSession.stopRunning()
            }
        }
    }
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }
    
    
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        
        return orientation
    }
    
    @objc func startCapture() {
        recordButton.isSelected = true
        startRecording()
        
    }
    
    //EDIT 1: I FORGOT THIS AT FIRST
    
    func tempURL() -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        
        if directory != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        
        return nil
    }
    
    
    func startRecording() {
        
        if movieOutput.isRecording == false {
            
            let connection = movieOutput.connection(with: AVMediaType.video)
            if (connection?.isVideoOrientationSupported)! {
                connection?.videoOrientation = currentVideoOrientation()
            }
            
            if (connection?.isVideoStabilizationSupported)! {
                connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }
            
            let device = activeInput.device
            if (device.isSmoothAutoFocusSupported) {
                do {
                    try device.lockForConfiguration()
                    device.isSmoothAutoFocusEnabled = false
                    device.unlockForConfiguration()
                } catch {
                    print("Error setting configuration: \(error)")
                }
                
            }
            
            //EDIT2: And I forgot this
            outputURL = tempURL()
            movieOutput.startRecording(to: outputURL, recordingDelegate: self)
            
        }
        else {
            stopRecording()
        }
        
    }
    
    func stopRecording() {
        if movieOutput.isRecording == true {
            recordButton.isSelected = false
            movieOutput.stopRecording()
        }
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        if (error != nil) {
            print("Error recording movie: \(error!.localizedDescription)")
        } else {
            
            _ = outputURL as URL
            
        }
        outputURL = nil
    }
}

extension CreateVideoPostViewController {
    func uploadVideo(){
        Loader.show()
        guard let caption = captionTextField.text else { return }

        var webName = String()
        var details = ["description": caption, "type": "video"]

        if isFromTask {
            webName = "tasks/submit/\(taskId)"
        } else {
            webName = "posts/upload"
            details = ["title": caption, "type": "video"]

        }
        do {
            let videoData = try Data(contentsOf: recordedVideoURL)
            Service.shared.multipartVideoUpload(webName: webName, imageData: videoData, imageName: "video", details: details, onCompletion: { (response) in
                print(response)
                self.navigationController?.popViewController(animated: true)
                self.showAlert(message: "Posted!")

                Loader.dismiss()
            }) { (err) in
                print(err)
                Loader.dismiss()
            }
        } catch {
            self.showAlert(message: error.localizedDescription)
        }
    }
}
