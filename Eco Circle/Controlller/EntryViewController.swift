//
//  EntryViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import TransitionButton
import Pastel

class EntryViewController: UIViewController {

    @IBOutlet weak var pastelView: PastelView!
    @IBOutlet weak var loginButton: TransitionButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: TransitionButton!
    
    lazy var registerVC = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
    lazy var feedVC = storyboard?.instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")

        if isLoggedIn {
            self.performSegue(withIdentifier: "goToHome", sender: self)

        }
//        emailTextField.layer.borderWidth = 1
//        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.cornerRadius = 3
        emailTextField.setLeftPaddingPoints(10)
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        emailTextField.backgroundColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4)
        
//        passwordTextField.layer.borderWidth = 1
//        passwordTextField.layer.borderColor = UIColor.lightGray.cgColor
        passwordTextField.layer.cornerRadius = 3
        passwordTextField.setLeftPaddingPoints(10)
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        passwordTextField.backgroundColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4)

        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4).cgColor
        loginButton.layer.cornerRadius = 3
        
        registerButton.layer.borderWidth = 1
        registerButton.layer.borderColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4).cgColor
        registerButton.layer.cornerRadius = 3
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getRandomColor()
        self.navigationController?.isNavigationBarHidden = true

    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    func getRandomColor() {
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
//        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
//                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
//                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
//                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
//                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
//                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
//                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        pastelView.setColors([UIColor(red:0.00, green:0.39, blue:0.00, alpha:1.0),
                              UIColor(red:0.49, green:0.99, blue:0.00, alpha:1.0),
                              UIColor(red:0.00, green:0.50, blue:0.00, alpha:1.0),
                              UIColor(red:0.20, green:0.80, blue:0.20, alpha:1.0)])
        
        pastelView.startAnimation()
    }
    
    
    @IBAction func loginButtonTapped(_ sender: TransitionButton) {
        if Common.sharedInst.haveInternet() {
            if isValid() { loginUser() } else { self.showAlert(message: no_valid)}
        } else { self.showAlert(message: no_internet) }
//        sender.startAnimation()
//        let qualityOfServiceClass = DispatchQoS.QoSClass.background
//        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
//        backgroundQueue.async(execute: {
//
//            sleep(3) // 3: Do your networking task or background work here.
//
//            DispatchQueue.main.async(execute: { () -> Void in
//                // 4: Stop the animation, here you have three options for the `animationStyle` property:
//                // .expand: useful when the task has been compeletd successfully and you want to expand the button and transit to another view controller in the completion callback
//                // .shake: when you want to reflect to the user that the task did not complete successfly
//                // .normal
//                sender.stopAnimation(animationStyle: .expand, completion: {
//                    self.present(self.registerVC, animated: true, completion: nil)
//                })
//            })
//        })
    }
    
    @IBAction func registerButtonTapped(_ sender: TransitionButton) {
//        sender.startAnimation()
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    func isValid() -> Bool {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return false }
        
        if !email.isEmpty && !password.isEmpty {
            return Common.sharedInst.validateEmailString(email: email)
        } else {
            return false
        }
    }
}

extension EntryViewController {
    func loginUser() {
        guard let email = emailTextField.text, let password = passwordTextField.text else { return }
        loginButton.startAnimation()
        let details = ["email": email, "password": password]
        Service.shared.request(webName: "login", whoswebcall: false, APIType: .post, params: details, completionBlock: { (response) in
            print(response)
            do {
                let userData = try jsonDecoder.decode(User.self, from: response as! Data)
                
                Common.sharedInst.saveUserData(data: userData)
                
                self.loginButton.stopAnimation(animationStyle: .expand, completion: {
                    self.navigationController?.pushViewController(self.feedVC, animated: true)
                })
                
                
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            
        }) { (error) in
            let errorData = error as! [String: String]
            if let error = errorData["error"] {
                self.showAlert(message: error)
            }
            self.loginButton.stopAnimation()
        }
    }
}
