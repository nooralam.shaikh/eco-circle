//
//  ProfileViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 21/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var userCollectionView: UICollectionView!
    
    var userData: User!
    var numberOfCells = 0
    var isFromFeed = false
    var userId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userCollectionView.delegate = self
        userCollectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        getUserProfile()
    }
    func getUserProfile() {
        Loader.show()
        var webName = "user-profile/view/\(userId)"
        if !isFromFeed {
          webName = "profile"
        }
        Service.shared.request(webName: webName, whoswebcall: false, APIType: .get, params: [:], completionBlock: { (response) in
            print(response)
            do {
                self.userData = try jsonDecoder.decode(User.self, from: response as! Data)
                self.setUserData()
                self.numberOfCells = 2
                self.userCollectionView.reloadData()
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
            Loader.dismiss()
        }) { (error) in
            let errorData = error as! [String: String]
            if let errorString = errorData["error"] {
                self.showAlert(message: errorString)
            }
            Loader.dismiss()
        }
    }
    
    func setUserData() {
        if let url = URL(string: userData.picture ?? "") {
            imageView.kf.setImage(with: url)
        }
        usernameLabel.text = userData.name
        emailLabel.text = userData.email
        phoneLabel.text = userData.mobile
        aboutLabel.text = userData.about
    }
    
}

extension ProfileViewController: UICollectionViewDelegate {
    
}

extension ProfileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileCell", for: indexPath) as! ProfileCollectionViewCell
        if indexPath.row == 0 {
            cell.imageView.image = UIImage(named: "coins")
            cell.titleLabel.text = "Rewards \(userData.rewards ?? 0)"
        } else {
            cell.imageView.image = UIImage(named: "badge")

            if let rank = userData.rank {
                switch rank {
                case 0...25:
                    cell.titleLabel.text = "Rookie"
                case 26...50:
                    cell.titleLabel.text = "Crafter"
                case 51...75:
                    cell.titleLabel.text = "Guru"
                default:
                    cell.titleLabel.text = "Grand Master"

                }
            }
        }
        return cell
    }
}
