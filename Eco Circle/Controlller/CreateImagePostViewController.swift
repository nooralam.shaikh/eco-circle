//
//  CreateImagePostViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 20/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import AVFoundation

class CreateImagePostViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var captionTextField: UITextField!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var tapLabel: UILabel!
    
    var isFromTask = false
    var taskId = Int()
    
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Capture Image"
        return title
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        postImageView.isUserInteractionEnabled = true
        postImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openCamera)))
        captionTextField.layer.cornerRadius = 3
        captionTextField.setLeftPaddingPoints(10)
        captionTextField.attributedPlaceholder = NSAttributedString(string: "Caption It!", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.83, green:0.83, blue:0.83, alpha:1)])
        captionTextField.backgroundColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:0.4)
        // Do any additional setup after loading the view.
    }
    
    func setNavigation() {
        let saveButton = UIBarButtonItem(image: UIImage(named: "checked"), style: .plain, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem  = saveButton
        
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backButtonTapped))
        self.navigationItem.leftBarButtonItem = backButton
        
        
        
        self.navigationItem.titleView = titleLabel
    }
    
    @objc func saveButtonTapped() {
        if Common.sharedInst.haveInternet() {
            if captionTextField.text != "" {
                uploadPost()
            }
        } else {
            self.showAlert(message: no_internet)
        }
//        Loader.show()
//        performSegue(withIdentifier: "goToHome", sender: self)
//        Loader.dismiss()
        
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK:- Image Picking methods
    @objc func openCamera() {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch cameraAuthorizationStatus {
        case .notDetermined: takePermissionForCameraUsage()
        case .authorized: presentCamera()
        case .restricted, .denied: alertCameraAccessNeeded()
        }
    }
    
    
    func takePermissionForCameraUsage() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentCamera()
        })
    }
    
    func presentCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            //            CommonSettings.sharedInst.setNavigationTitle(title: "Camera", navigationItem: imagePicker.navigationItem)
            self.present(imagePicker, animated: true, completion: nil)
        } else if !UIImagePickerController.isSourceTypeAvailable(.camera){
            //            CommonSettings.sharedInst.showError(description: "Device has no camera", viewController: self)
        }
    }
    
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alert = UIAlertController(
            title: "Need Camera Access",
            message: "Camera access is required to update profile image.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            } else {
                self.showAlert(message: "This app does not have access to Camera. You can enable access in Privacy Settings.")
                // Fallback on earlier versions
                //                CommonSettings.sharedInst.showError(description: "This app does not have access to Camera. You can enable access in Privacy Settings.", viewController: self)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
}

extension CreateImagePostViewController: UIImagePickerControllerDelegate {
    //MARK:- Image Picker Delegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            postImageView.image = pickedImage
            tapLabel.isHidden = true
//            postImageView.contentMode = .scaleAspectFill
            dismiss(animated: true, completion: nil)
        }
    }
}

extension CreateImagePostViewController {
    func uploadPost() {
        Loader.show()
        var webName = String()
        guard let image = postImageView.image, let caption = captionTextField.text else { return }

        var details = ["description": caption, "type": "image"]
        if isFromTask {
            webName = "tasks/submit/\(taskId)"
        } else {
            details = ["title": caption, "type": "image"]
            webName = "posts/upload"
        }
        
        if let imageData = image.jpegData(compressionQuality: 0.5) {
            Service.shared.postUpload(webName: webName, imageData: imageData, details: details, onCompletion: { (response) in
                print(response)
                self.navigationController?.popViewController(animated: true)
                self.showAlert(message: "Posted!")
                Loader.dismiss()
            }) { (err) in
                print(err)
                Loader.dismiss()
            }
        }
        
    }
}
