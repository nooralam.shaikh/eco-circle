//
//  TakingUserDataViewController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import AVFoundation
import Photos


class TakingUserDataViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var aboutTextView: JVFloatLabeledTextView!
    
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Profile"
        return title
    }()
    lazy var feedView = storyboard?.instantiateViewController(withIdentifier: "BaseTabBarController") as! BaseTabBarController
    override func viewDidLoad() {
        super.viewDidLoad()
        let saveButton = UIBarButtonItem(image: UIImage(named: "checked"), style: .plain, target: self, action: #selector(saveButtonTapped))
        self.navigationItem.rightBarButtonItem  = saveButton
        
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backButtonTapped))
        self.navigationItem.leftBarButtonItem = backButton
        
        
        
        self.navigationItem.titleView = titleLabel
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        userProfileImageView.isUserInteractionEnabled = true
        userProfileImageView.addGestureRecognizer(tapGestureRecognizer)
        
        userProfileImageView.layer.cornerRadius = userProfileImageView.frame.height / 2
        userProfileImageView.layer.masksToBounds = true
    }
    
    @objc func saveButtonTapped() {
//        Loader.show()
//        performSegue(withIdentifier: "goToHome", sender: self)
        uploadImage()
//        Loader.dismiss()
        
    }
    
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        // Your action
        
        let actionSheetController = UIAlertController(title: "Edit Image", message: "", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            //dismiss action
//            DLog(message: "dismiss pressed")
        }
        
        actionSheetController.addAction(cancelAction)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            //go to camera
            self.openCamera()
        }
        actionSheetController.addAction(cameraAction)
        
        let photosAction = UIAlertAction(title: "Photos", style: .default) { (_) in
            //go to Photos
            self.openPhotos()
        }
        actionSheetController.addAction(photosAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK:- Image Picking methods
    func openCamera() {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch cameraAuthorizationStatus {
        case .notDetermined: takePermissionForCameraUsage()
        case .authorized: presentCamera()
        case .restricted, .denied: alertCameraAccessNeeded()
        }
    }
    func takePermissionForCameraUsage() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentCamera()
        })
    }
    
    func presentCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
//            CommonSettings.sharedInst.setNavigationTitle(title: "Camera", navigationItem: imagePicker.navigationItem)
            self.present(imagePicker, animated: true, completion: nil)
        } else if !UIImagePickerController.isSourceTypeAvailable(.camera){
//            CommonSettings.sharedInst.showError(description: "Device has no camera", viewController: self)
        }
    }
    
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alert = UIAlertController(
            title: "Need Camera Access",
            message: "Camera access is required to update profile image.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            } else {
                self.showAlert(message: "This app does not have access to Camera. You can enable access in Privacy Settings.")
                // Fallback on earlier versions
//                CommonSettings.sharedInst.showError(description: "This app does not have access to Camera. You can enable access in Privacy Settings.", viewController: self)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func openPhotos() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            
            imagePicker.navigationBar.barTintColor = UIColor(red:0.0, green:0.3, blue:0.00, alpha:1.0)
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.navigationBar.shadowImage = UIImage()
            imagePicker.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            imagePicker.navigationBar.tintColor = UIColor.white
//            imagePicker.navigationItem.titleView = titleLabel
            //CommonSettings.sharedInst.setNavigationTitle(title: "Photos", navigationItem: imagePicker.navigationItem)
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            alertPhotosAccessNeeded()
        }
    }
    
    func alertPhotosAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alert = UIAlertController(
            title: "Need Photos Access",
            message: "Photos access is required to update profile image.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
//                CommonSettings.sharedInst.showError(description: "This app does not have access to Photos. You can enable access in Privacy Settings.", viewController: self)
                self.showAlert(message: "This app does not have access to Photos. You can enable access in Privacy Settings.")
            }
        }))
        present(alert, animated: true, completion: nil)
    }

    

}

extension TakingUserDataViewController: UIImagePickerControllerDelegate {
    //MARK:- Image Picker Delegate Methods

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userProfileImageView.image = pickedImage
            userProfileImageView.contentMode = .scaleAspectFill
            dismiss(animated: true, completion: nil)
        }
    }
}

extension TakingUserDataViewController {
    func uploadImage() {
        Loader.show()
        if let image = userProfileImageView.image {
            if let imageData =  image.jpegData(compressionQuality: 0.5) {
                Service.shared.multipartImageUpload(webName: "profile", imageData: imageData, onCompletion: { (response) in
                    print(response)
                    
                    Loader.dismiss()
                    self.navigationController?.pushViewController(self.feedView, animated: true)
                }) { (err) in
                    Loader.dismiss()
                    self.navigationController?.pushViewController(self.feedView, animated: true)

                    print(err)
                }
            }
        }
    }
}
