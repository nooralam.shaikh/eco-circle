/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Task : Decodable {
	let ngo_name : String?
	let id : Int?
	let title : String?
	let location : String?
	let instructions : String?
	let description : String?
	let end_date : String?
	let created_at : String?
	let submitted : Int?
	let participated : Int?
	let submissions : Int?

	enum CodingKeys: String, CodingKey {

		case ngo_name = "ngo_name"
		case id = "id"
		case title = "title"
		case location = "location"
		case instructions = "instructions"
		case description = "description"
		case end_date = "end_date"
		case created_at = "created_at"
		case submitted = "submitted"
		case participated = "participated"
		case submissions = "submissions"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		ngo_name = try values.decodeIfPresent(String.self, forKey: .ngo_name)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		location = try values.decodeIfPresent(String.self, forKey: .location)
		instructions = try values.decodeIfPresent(String.self, forKey: .instructions)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		submitted = try values.decodeIfPresent(Int.self, forKey: .submitted)
		participated = try values.decodeIfPresent(Int.self, forKey: .participated)
		submissions = try values.decodeIfPresent(Int.self, forKey: .submissions)
	}
    

}
