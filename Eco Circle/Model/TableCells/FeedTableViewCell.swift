//
//  FeedTableViewCell.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 20/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var postCreateLabel: UILabel!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var postThumbnailImageView: UIImageView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewHeight: NSLayoutConstraint!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var cellInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = userImageView.frame.height / 2
        userImageView.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
