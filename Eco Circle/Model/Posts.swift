//
//  Posts.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 21/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import Foundation
struct Post : Decodable {
    let posts : [Posts]?
    
    enum CodingKeys: String, CodingKey {
        
        case posts = "posts"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posts = try values.decodeIfPresent([Posts].self, forKey: .posts)
    }
    
}
