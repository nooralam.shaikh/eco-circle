//
//  UIViewControllerExtension.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String = "", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        present(alert, animated: true, completion: nil)
    }
}
