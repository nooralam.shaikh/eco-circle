//
//  BaseTabBarController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 19/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    lazy var redeemViewController = storyboard?.instantiateViewController(withIdentifier: "RedeemViewController") as! RedeemViewController
    lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.textColor = UIColor.white
        title.font = Common.sharedInst.getFont(type: .regular, size: 17)
        title.text = "Eco Circle"
        return title
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let saveButton = UIBarButtonItem(image: UIImage(named: "qr-code"), style: .plain, target: self, action: #selector(qrcodeTapped))
        self.navigationItem.rightBarButtonItem  = saveButton
        self.navigationItem.titleView = titleLabel
//        self.navigationItem.leftBarButtonItem = nil
//        self.navigationController?.navigationItem.leftBarButtonItem = nil
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
    }
    @objc func qrcodeTapped() {
        self.navigationController?.pushViewController(redeemViewController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
