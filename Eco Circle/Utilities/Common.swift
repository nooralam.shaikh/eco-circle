//
//  Common.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import Foundation
import UIKit
import Reachability

private let sharedInstance = Common()
    
class Common: NSObject {
    
    let reachability = Reachability()!

    class var sharedInst: Common {
        return sharedInstance
    }
    var userInfoDict = [String:Any]()
    var defaults = UserDefaults.standard
    
    

}
extension Common{
    
    //MARK:- User Defaults Methods
    
    func saveUserData(data userData: User) {
        
        UserDefaults.standard.set(userData.token, forKey: "token")
        UserDefaults.standard.set(userData.id, forKey: "userId")
        UserDefaults.standard.set(userData.mobile, forKey: "mobile")
        UserDefaults.standard.set(userData.email, forKey: "email")
        UserDefaults.standard.set(userData.area, forKey: "area")
        UserDefaults.standard.set(userData.rank, forKey: "rank")
        UserDefaults.standard.set(userData.rewards, forKey: "rewards")
        UserDefaults.standard.set(userData.areaId, forKey: "areaId")
        UserDefaults.standard.set(userData.picture, forKey: "picture")
        UserDefaults.standard.set(userData.name, forKey: "name")

        UserDefaults.standard.set(true, forKey: "isLoggedIn")
        
    }
    
    func getUserData(for key: String) -> Any? {
        return UserDefaults.standard.data(forKey: key)
        
    }
}

extension Common{
    
    //MARK:- Reachability Methods
    
    func haveInternet() -> Bool{
        
        var flag = false
        NetworkManager.isReachable { (_) in
            flag = true
        }
        return flag
    }
}

extension Common{
    //MARK:- Validations Email and Mobile
    
    func validateEmailString(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func validateMobileNo(validateNumber: String) -> Bool {
        let numberRegEx = "[0-9]{10}"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        return numberTest.evaluate(with: validateNumber)
    }
    
    
}

//MARK:- Font Function
enum FontType: String {
    case regular = "Poppins-Regular"
    case medium = "Poppins-Medium"
    case bold = "Poppins-Bold"
}

extension Common {
    
    func getFont(type: FontType, size: CGFloat) -> UIFont {
        return UIFont(name: type.rawValue, size: size)!
    }
    
}
