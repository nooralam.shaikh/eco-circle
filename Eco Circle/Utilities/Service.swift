//
//  Service.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import Foundation
import Alamofire

class Service: NSObject {
    static let shared = Service()
    
    var header = [
        "content-type": "application/json",
        "Authorization": ""
    ]
    
    func checkToken(accessToken: String?){
        if let token = accessToken {
            header["Authorization"] = "Bearer " + token
        }
    }
    
    
    func request(webName: String,whoswebcall:Bool, APIType: HTTPMethod, params: [String:Any], completionBlock:@escaping(Any) -> (), error:@escaping(Any) -> ()){
        let URL = base_url + webName
        
        var encoding = whoswebcall
        
        if(APIType == .post || APIType == .delete || APIType == .patch){
            header = [:]
            encoding = true
        }
        
        checkToken(accessToken: UserDefaults.standard.value(forKey: "token") as? String)        
        Alamofire.request(URL.trimmingCharacters(in: .whitespacesAndNewlines), method: APIType, parameters: params, encoding: encoding ? JSONEncoding.default : URLEncoding.httpBody, headers: header).responseJSON { (response) in
            let statusCode = response.response?.statusCode
            if statusCode == 401 {
                switch response.result {
                case .success(let JSON):
                    error(JSON)
                case .failure(let JSON):
                    error(JSON)
                }
            }  else if statusCode == 200 {
                switch response.result {
                case .success(let JSON):
                    if(whoswebcall){
                        completionBlock(JSON)
                    } else{
                        print(JSON)
                        completionBlock(response.data!)
                    }
                case .failure(let err):
                    completionBlock(err)
                }
            } else if statusCode == 400 {
                switch response.result {
                case .success(let JSON):
                    error(JSON)
                case .failure(let err):
                    error(err)
                }
            } else if statusCode == 422 {
                switch response.result {
                case .success(let JSON):
                    error(JSON)
                case .failure(let JSON):
                    error(JSON)
                }
            }
        }
    }
    
    func multipartImageUpload(webName: String, imageData: Data?,imageName: String = "", onCompletion: @escaping (Any) -> (), onError: @escaping (Any) -> ()){
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let contentType = "multipart/form-data; boundary=\(deviceID)"
        checkToken(accessToken: UserDefaults.standard.value(forKey: "token") as? String)
        header["content-type"] = contentType
        let url = base_url + webName
        var parameters = [String:Any]()
        parameters = ["picture": imageName,"mimetype":"image/jpeg"]
        let name = UserDefaults.standard.value(forKey: "name")
        let area = UserDefaults.standard.value(forKey: "areaId")
        parameters["area"] = area as? Int
        parameters["name"] = name as? String
        
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value ?? "")".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data, withName: "picture", fileName: "image1.jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: header) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error {
                        onError(err)
                    }
                    onCompletion(response)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError(error)
            }
        }
    }
    
    func postUpload(webName: String, imageData: Data?,imageName: String = "",details: [String:String], onCompletion: @escaping (Any) -> (), onError: @escaping (Any) -> ()){
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let contentType = "multipart/form-data; boundary=\(deviceID)"
//        if let userToken = Common.sharedInst.getUserData(for: "token") as! String? {
//            checkToken(accessToken: userToken)
//        }
        checkToken(accessToken: UserDefaults.standard.value(forKey: "token") as? String)

        header["content-type"] = contentType
        let url = base_url + webName
        var parameters = details
        parameters["p_file"] = imageName
        parameters["mimetype"] = "image/jpeg"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data, withName: "p_file", fileName: "image1.jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: header) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error {
                        onError(err)
                    }
                    onCompletion(response)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError(error)
            }
        }
    }
    
    func multipartVideoUpload(webName: String, imageData: Data?,imageName: String = "", details: [String: Any], onCompletion: @escaping (Any) -> (), onError: @escaping (Any) -> ()){
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let contentType = "multipart/form-data; boundary=\(deviceID)"
        checkToken(accessToken: UserDefaults.standard.value(forKey: "token") as? String)
        header["content-type"] = contentType
        let url = base_url + webName
        var parameters = details
        parameters["p_file"] = "salma.mp4"
//        parameters["mimetype"] = "video/mp4"
//        parameters["name"] = UserDefaults.standard.value(forKey: "name") as? String
//        parameters["area"] = UserDefaults.standard.value(forKey: "area")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data, withName: "p_file", fileName: "salma.mp4", mimeType: "video/mp4")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: header) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error {
                        onError(err)
                    }
                    onCompletion(response)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError(error)
            }
        }
    }
}

/*
{
"success": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFiYzQwMDU5OGYzNzE2MDNiZDdkMzYxNWMxZjA2MzY3ODk2NDQxZjMxZDM0ZDBjMzI1MzY3YTNlNDgzNjA1MjgxYjgwMzQ0MzJhZTFjMWUyIn0.eyJhdWQiOiIxIiwianRpIjoiYWJjNDAwNTk4ZjM3MTYwM2JkN2QzNjE1YzFmMDYzNjc4OTY0NDFmMzFkMzRkMGMzMjUzNjdhM2U0ODM2MDUyODFiODAzNDQzMmFlMWMxZTIiLCJpYXQiOjE1Mzk4Nzk4NzgsIm5iZiI6MTUzOTg3OTg3OCwiZXhwIjoxNTcxNDE1ODc4LCJzdWIiOiI1Iiwic2NvcGVzIjpbXX0.GoIPjRrzqkxwk2VkXHN8-fBN7HC3zuTkMEfHEP1-ZBBl3PtM8IV-EzcBpYPMmbab7HWJfkhHFIR0g-pDSUFcPQR8MWhSEcrRmTD-b5Y_NTcGPfg7Y5cZbxf97FPh95jPY-hllfN5wjOfreSbi7-pCB7v9md44uR18yRjPODbU0onTdJztN4oAcZyNw2qsg9LriAQsBfqzxfNx2zKanGqpk3EoZnN1CmvETRClcpPwgjn-2Ph9EaPtzlVw03kc8degs2JdAiqfQvOi27DvdiXPBUj-icFzmxGHgRGhKI9cP4Pnd-4-46UhmASPi1XLs7we38GFvnJ2WrT6yoxZn4vRLZkLsYn7oGTSMPxhW6tLXJHYPFLF3MT6LuwQRBKUoD_57MZRDAfQxhuQ3neOnmck0l7CDr8TnBNby7GDRe9SyWFROaJbPkeLStu4BJ25W5ReIZWPnVvCyjIeQx9WXlNCYSit8EGwHLJSIvhJ8KhBdFjeP2vpfubdJVvICcKJXQH-XcUiN3pKo3byX97Nmj-qKJVL-k5gGCFyfWyvmbLVqfwuVLYCP4RNAoD_exRvmP5yILPu9oVPlYpkyIPwEc3Az_NzW9SznE2VbQSoPGc7qtb_WomGbMXulVaQi0tcSeM59WcHOjBAuW7X1-Tolia56cNxuapJ-4qntzxnn6h8R8",
            "user": {
                "email": "aaba@aaa.com",
                "mobile": "1234567812",
                "name": "noooor",
                "updated_at": "2018-10-18 16:24:38",
                "created_at": "2018-10-18 16:24:38",
                "id": 5
                }
            }
}
 
*/
