//
//  Constant.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

typealias Loader = SVProgressHUD

let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//let base_url = "http://ecocircle.ml/api/"
let base_url = "http://192.168.43.6:8000/api/"

let no_internet = "No internet connection, Make sure Wi-Fi or cellular data is turned on, then try again."
let no_valid = "Please enter valid input."

let COLOR_FOREST_GREEN = UIColor(red:0.13, green:0.55, blue:0.13, alpha:1.0)

let jsonDecoder = JSONDecoder()

