//
//  BaseNavigationController.swift
//  Eco Circle
//
//  Created by Nooralam Shaikh on 18/10/18.
//  Copyright © 2018 Nooralam Shaikh. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.tintColor = UIColor.white//(red:0.49, green:0.99, blue:0.00, alpha:1.0)
        self.navigationBar.barTintColor = UIColor(red:0.0, green:0.3, blue:0.00, alpha:1.0)
        self.navigationController?.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Futura", size: 20)!], for: .normal)

        // Do any additional setup after loading the view.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
